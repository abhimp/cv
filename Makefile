TEXFILES  = $(wildcard *.tex)
TARGETS = $(patsubst %.tex,%.pdf,$(TEXFILES))

.PHONY: all clean

all: $(TARGETS)

%.pdf: %.tex
	pdflatex -interaction=nonstopmode -halt-on-error $<
	pdflatex -interaction=nonstopmode -halt-on-error $<
	pdflatex -interaction=nonstopmode -halt-on-error $<

cp: ${TARGETS}
	cp ${TARGETS} /tmp/


clean:
	rm -fv *.dvi *.aux *.log *~ *.bbl *.blg *.toc *.ps *.pdf *.ent
